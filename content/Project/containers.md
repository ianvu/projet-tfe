---
title: "Le container"
subtitle: ""
image_teaser: "/project/images/formes/container.jpg"
image_background: "/project/images/formes/container.jpg"
summary: "Simple boite métallique à tout faire, le container est un outil modulaire et pratique que l'on peut agencer à l'infini."
website: ""
year: 2017
slug: "container"
featured: true
tags: [ Événementialisé, Grande distribution, 1 jours à 3 semaines, Grand espace ]
filters: [ Structure commerciale, evenementiality, accessibility ]
evenementiality: false
accessibility: true
paragraph: [
    {
        id: client,
        class: [ constrained ],
        text_big: "<b>Simple boite métallique à tout faire, </b>le container est un outil modulaire et pratique que l'on peut agencer à l'infini.",
        title_left: "Type de vente",
        text_left: "Bien choisir son espace de vente est important pour la suite de vos événements.",

        type: "Espace public",

        title_right: "De l’espace pour votre clientèle, vos marchandises, l’article phare de votre marque.",
        text_right: "Selon ce que vous voulez présenter ou mettre en avant, la superficie reste un point important pour la recherche d’un emplacement.",
        item: [
            {
                title: "Durée</br> favorable :",
                value: "1 jour à 3 semaines"
            },
            {
                title: "Loyer</br> approximatif :",
                value: "50 à 300€ / jour"
            },
            {
                title: "Surface</br> commerciale :",
                value: "Prévoir un emplacement"
            }
        ]
    },
    {
        id: concept,
        class: [ text, cols-2 ],
        text_big: '<b>Ce qui est possible de faire avec ce </b>type de surface commerciale',
        text: 'Facilement transportables, ils permettent d’investir successivement plusieurs lieux.'
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/formes/container-01.jpg"
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/formes/container-02.jpg"
    }
]
---
