---
title: "Les centres commerciaux"
subtitle: ""
image_teaser: "/project/images/lieux/centre-commerciaux.jpg"
image_background: "/project/images/lieux/centre-commerciaux.jpg"
summary: "Au cœur d'une allée, dans un espace vacant, sur un parking, dans les jardins... les pop-up stores n'ont que l'embarras du choix pour profiter du flux généré par les centres commerciaux."
website: ""
year: 2017
slug: "centre-commercial"
featured: true
tags: [ Événementialisé, Accessibilisé, Grande distribution, 3 mois à 6 mois, Centre commercial, Destockage ]
filters: [ Lieu d'exploitation ]
evenementiality: true
accessibility: false
paragraph: [
    {
        id: client,
        class: [ constrained ],
        text_big: "<b>Au cœur d'une allée, dans un espace vacant, sur un parking, dans les jardins... </b>les pop-up stores n'ont que l'embarras du choix pour profiter du flux généré par les centres commerciaux.",
        title_left: "Type de vente",
        text_left: "Bien choisir son espace de vente est important pour la suite de vos événements.",

        type: "Espace commercial",

        title_right: "De l’espace pour votre clientèle, vos marchandises, l’article phare de votre marque.",
        text_right: "Selon ce que vous voulez présenter ou mettre en avant, la superficie reste un point important pour la recherche d’un emplacement.",
        item: [
            {
                title: "Durée</br> favorable :",
                value: "3 mois à 6 mois"
            },
            {
                title: "Loyer</br> approximatif :",
                value: "500 à 2500 € / mois"
            },
            {
                title: "Surface</br> commerciale :",
                value: "30 à 200 m2"
            }
        ]
    },
    {
        id: concept,
        class: [ text, cols-2 ],
        text_big: "<b>Une totale créativité avec ce </b>type de d'implantation",
        text: 'A remplir'
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/portfolio-aqua-publica-01.jpg"
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/portfolio-aqua-publica-02.jpg"
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/portfolio-aqua-publica-03.jpg"
    }
]
---
