---
title: "Les grands magasins"
subtitle: ""
image_teaser: "/project/images/lieux/grand-magasin.jpg"
image_background: "/project/images/lieux/grand-magasin.jpg"
summary: "Espace de démonstration ou simple espace de vente, dans les grands magasins, les marques utilisent les stands dédiés pour des actions de notoriété ciblées, « so chic ! »"
website: ""
year: 2017
slug: "grand-magasin"
featured: true
tags: [ Événementialisé, Grande distribution, Destockage ]
filters: [ Lieu d'exploitation ]
paragraph: [
    {
        id: client,
        class: [ constrained ],
        text_big: "<b>Espace de démonstration ou simple espace de vente, </b>dans les grands magasins, les marques utilisent les stands dédiés pour des actions de notoriété ciblées, « so chic ! »",
        text_left: "Bien choisir son espace de vente est important pour la suite de vos événements.",

        type: "Espace commercial",

        title_right: "De l’espace pour votre clientèle, vos marchandises, l’article phare de votre marque.",
        text_right: "Selon ce que vous voulez présenter ou mettre en avant, la superficie reste un point important pour la recherche d’un emplacement.",
        item: [
            {
                title: "Durée</br> favorable :",
                value: "1 jour à 6 mois"
            },
            {
                title: "Loyer</br> approximatif :",
                value: "50 à 150€ / jours"
            },
            {
                title: "Surface</br> commerciale :",
                value: "1 à 200 m2"
            }
        ]
    },
    {
        id: concept,
        class: [ text, cols-2 ],
        text_big: "<b>Une totale créativité avec ce </b>type de d'implantation",
        text: 'A remplir'
    }
]
---
