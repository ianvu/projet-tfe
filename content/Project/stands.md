---
title: "Les établis"
subtitle: ""
image_teaser: "/project/images/formes/etabli.jpg"
image_background: "/project/images/formes/etabli.jpg"
summary: "Très prisé par les grandes entreprises qui décident de faire voyager leur produit facilement dans toutes sortent d’endroits."
website: ""
year: 2017
slug: "stands"
featured: true
tags: [ Accessibilisé, Événementialisé, Grande distribution, 1 mois à 3 mois, Centre commercial, Grand espace ]
filters: [ Structure commerciale ]
paragraph: [
    {
        id: client,
        class: [ constrained ],
        text_big: "<b>Très prisé</b> par les grandes entreprises qui décident de faire voyager leur produit facilement dans toutes sortent d’endroits.",
        title_left: "Type de vente",
        text_left: "Bien choisir son espace de vente est important pour la suite de vos événements.",

        type: "Kiosque ou stand",

        title_right: "De l’espace pour votre clientèle, vos marchandises, l’article phare de votre marque.",
        text_right: "Selon ce que vous voulez présenter ou mettre en avant, la superficie reste un point important pour la recherche d’un emplacement.",
        item: [
            {
                title: "Durée</br> favorable :",
                value: "1 mois à 3 mois"
            },
            {
                title: "Loyer</br> approximatif :",
                value: "50 - 100€ / jour"
            },
            {
                title: "Surface</br> commerciale :",
                value: "1 à 100 m2"
            }
        ]
    },
    {
        id: concept,
        class: [ text, cols-2 ],
        text_big: '<b>Ce qui est possible de faire avec ce </b>type de surface commerciale',
        text: 'Créez vous aussi des établis aux couleurs de la marque pour attirer intuitivement le client vers le présentoir et lui proccurer une satisfaction de recherche plus direct ou une découverte plus importante du produit.'
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/formes/etabli-01.jpg"
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/formes/etabli-02.jpg"
    }
]
---
