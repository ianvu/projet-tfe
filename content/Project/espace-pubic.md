---
title: "L'espace public, espace vacances"
subtitle: ""
image_teaser: "/project/images/lieux/espace-public.jpg"
image_background: "/project/images/lieux/espace-public.jpg"
summary: "Dans un jardin, sur un quai, au cœur d'une place, au bord d'une plage... la rue en fonction de son emplacement est un lieu fantastique pour aller à la rencontre des consommateurs. Selon l'univers de la marque, la proximité d'un monument emblématique sera recherchée (marques de luxe)."
website: ""
year: 2017
slug: "espace-public"
featured: true
tags: [ Événementialisé, Grande distribution, 1 jour à 3 semaines, Grand espace ]
filters: [ Lieu d'exploitation ]
evenementiality: true
accessibility: true
paragraph: [
    {
        id: client,
        class: [ constrained ],
        text_big: "<b>Dans un jardin, sur un quai, au cœur d'une place... </b>la rue est un lieu fantastique pour aller à la rencontre des consommateurs. Selon l'univers de la marque, la proximité d'un monument emblématique sera recherchée (marques de luxe).",
        title_left: "Type de vente",
        text_left: "Bien choisir son espace de vente est important pour la suite de vos événements.",

        type: "Espace public",

        title_right: "De l’espace pour votre clientèle, vos marchandises, l’article phare de votre marque.",
        text_right: "Selon ce que vous voulez présenter ou mettre en avant, la superficie reste un point important pour la recherche d’un emplacement.",
        item: [
            {
                title: "Durée</br> favorable :",
                value: "1 jour à 3 semaines"
            },
            {
                title: "Loyer</br> approximatif :",
                value: "Selon l'espace et l'environnement"
            },
            {
                title: "Surface</br> commerciale :",
                value: "1 à 100 m2"
            }
        ]
    },
    {
        id: concept,
        class: [ text, cols-2 ],
        text_big: "<b>Une totale créativité avec ce </b>type de d'implantation",
        text: 'Le simulacre peut jouer sur différents ressorts (historiques, naturels, fantastiques, etc).'
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/portfolio-aqua-publica-01.jpg"
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/portfolio-aqua-publica-02.jpg"
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/portfolio-aqua-publica-03.jpg"
    }
]
---
