---
title: "Les bars, restaurants"
subtitle: ""
image_teaser: "/project/images/formes/restaurant.jpg"
image_background: "/project/images/formes/restaurant.jpg"
summary: "Convivial par nature, le bar et le restaurant sont des lieux animés où l'on peut organiser facilement des animations et des dégustations."
website: ""
year: 2017
slug: "horeca"
featured: true
tags: [ Accessibilisé, Indépendant, Grande distribution, 6 mois à 1 an, Cellule vide ]
filters: [ Structure commerciale ]
paragraph: [
    {
        id: client,
        class: [ constrained ],
        text_big: "<b>C’est un lieu de sociabilité, </b>de convivialité et de plaisir, un espace de vie transitoire, hors de chez soi.",
        title_left: "Type de vente",
        text_left: "Bien choisir son espace de vente est important pour la suite de vos événements.",

        type: "Cellule commerciale",

        title_right: "De l’espace pour votre clientèle, vos marchandises, l’article phare de votre marque.",
        text_right: "Selon ce que vous voulez présenter ou mettre en avant, la superficie reste un point important pour la recherche d’un emplacement.",
        item: [
            {
                title: "Durée</br> favorable :",
                value: "6 mois à 1 an"
            },
            {
                title: "Loyer</br> approximatif :",
                value: "500 à 1200€ / mois"
            },
            {
                title: "Surface</br> commerciale :",
                value: "À compter 1 à 1,5m2 par place"
            }
        ]
    },
    {
        id: concept,
        class: [ text, cols-2 ],
        text_big: '<b>Ce qui est possible de faire avec ce </b>type de surface commerciale',
        text: 'A remplir'
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/formes/restaurant-01.jpg"
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/formes/restaurant-02.jpg"
    }
]
---
