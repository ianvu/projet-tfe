---
title: "Les lieux de transport"
subtitle: ""
image_teaser: "/project/images/lieux/transit.png"
image_background: "/project/images/lieux/transit.png"
summary: "Les lieux de trafics intenses que sont les hubs urbains offrent un emplacement de choix pour les marques."
website: ""
year: 2017
slug: "transit"
featured: true
tags: [ Événementialisé, Grande distribution, 1 jour à 3 semaines, Centre commercial ]
filters: [ Lieu d'exploitation ]
paragraph: [
    {
        id: client,
        class: [ constrained ],
        text_big: "<b>À l’intérieur, </b>les pop-up stores revêtent la forme de corners ou de stands. <b>À l’extérieur, </b>les parvis peuvent accueillir des dispositifs de grande envergure.",
        title_left: "Type de vente",
        text_left: "Bien choisir son espace de vente est important pour la suite de vos événements.",

        type: "Lieux insolites",

        title_right: "De l’espace pour votre clientèle, vos marchandises, l’article phare de votre marque.",
        text_right: "Selon ce que vous voulez présenter ou mettre en avant, la superficie reste un point important pour la recherche d’un emplacement.",
        item: [
            {
                title: "Durée</br> favorable :",
                value: "1 jour à 3 semaines"
            },
            {
                title: "Loyer</br> approximatif :",
                value: "50 à 200€ / jour"
            },
            {
                title: "Surface</br> commerciale :",
                value: "1 à 100 m2"
            }
        ]
    },
    {
        id: concept,
        class: [ text, cols-2 ],
        text_big: "<b>Une totale créativité avec ce </b>type de d'implantation",
        text: 'A remplir'
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/lieux/transit-01.jpg"
    }
]
---
