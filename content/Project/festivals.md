---
title: "Les festivals"
subtitle: ""
image_teaser: "/project/images/lieux/festival.jpeg"
image_background: "/project/images/lieux/festival.jpeg"
summary: "Qu'ils soient dédiés à la musique ou à une activité sportive particulière, les festivals permettent d'aborder l'installation d'un pop-up store thématique sous l'angle communautaire."
website: ""
year: 2017
slug: "festivals"
featured: true
tags: [ Événementialisé, Grande distribution, 1 jour à 3 semaines, Grand espace ]
filters: [ Lieu d'exploitation ]
paragraph: [
    {
        id: client,
        class: [ constrained ],
        text_big: "<b>Qu'ils soient dédiés à la musique ou à une activité sportive particulière, </b>les festivals permettent d'aborder l'installation d'un pop-up store thématique sous l'angle communautaire. ",
        text_left: "Bien choisir son espace de vente est important pour la suite de vos événements.",

        type: "Espace événementiel",

        title_right: "De l’espace pour votre clientèle, vos marchandises, l’article phare de votre marque.",
        text_right: "Selon ce que vous voulez présenter ou mettre en avant, la superficie reste un point important pour la recherche d’un emplacement.",
        item: [
            {
                title: "Durée</br> favorable :",
                value: "1 jour à 3 semaines"
            },
            {
                title: "Loyer</br> approximatif :",
                value: "Selon l'espace et l'environnement"
            },
            {
                title: "Surface</br> commerciale :",
                value: "1 à 200 m2"
            }
        ]
    },
    {
        id: concept,
        class: [ text, cols-2 ],
        text_big: "<b>Une totale créativité avec ce </b>type de d'implantation",
        text: 'Généralement, les pop-up stores sont regroupés en village dans ce type de manifestation.'
    }
]
---
