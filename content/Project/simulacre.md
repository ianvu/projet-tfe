---
title: "Le simulacre"
subtitle: ""
image_teaser: "/project/images/formes/simulacre.jpg"
image_background: "/project/images/formes/simulacre.jpg"
summary: "En recréant une ambiance particulière (olfactive, auditive, etc), l'installation éphémère plonge le consommateur dans un total dépaysement dans l'univers de la marque."
website: ""
year: 2017
slug: "simulacre"
featured: true
tags: [ Événementialisé, Indépendant, Grande distribution, 1 jours à 3 semaines, Grand espace, Centre commercial ]
filters: [ Structure commerciale ]
paragraph: [
    {
        id: client,
        class: [ constrained ],
        text_big: "<b>En recréant une ambiance particulière (olfactive, auditive, etc), </b>l'installation éphémère plonge le consommateur dans un total dépaysement dans l'univers de la marque.",
        title_left: "Type de vente",
        text_left: "Bien choisir son espace de vente est important pour la suite de vos événements.",

        type: "Espace public",

        title_right: "De l’espace pour votre clientèle, vos marchandises, l’article phare de votre marque.",
        text_right: "Selon ce que vous voulez présenter ou mettre en avant, la superficie reste un point important pour la recherche d’un emplacement.",
        item: [
            {
                title: "Durée</br> favorable :",
                value: "1 jour à 3 semaines"
            },
            {
                title: "Loyer</br> approximatif :",
                value: "25 à 400€ / jour"
            },
            {
                title: "Surface</br> commerciale :",
                value: "1 à 100 m2"
            }
        ]
    },
    {
        id: concept,
        class: [ text, cols-2 ],
        text_big: '<b>Ce qui est possible de faire avec ce </b>type de surface commerciale',
        text: 'Le simulacre peut jouer sur différents ressorts (historiques, naturels, fantastiques, etc).'
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/lieux/simulacre-01.jpg"
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/lieux/simulacre-02.jpg"
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/lieux/simulacre-03.jpg"
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/lieux/simulacre-04.jpg"
    }
]
---
