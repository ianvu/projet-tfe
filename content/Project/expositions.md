---
title: "L'exposition"
subtitle: ""
image_teaser: "/project/images/formes/exposition.jpg"
image_background: "/project/images/formes/exposition.jpg"
summary: "De durée limitée, l'exposition permet d'associer l'univers de la marque à des talents artistiques extérieurs pour un moment de partage culturel sur un mode proche du mécénat."
website: ""
year: 2017
slug: "exposition"
featured: true
tags: [ Événementialisé, Indépendant, Grande distribution, 1 jours à 3 semaines, Grand espace ]
filters: [ Structure commerciale ]
paragraph: [
    {
        id: client,
        class: [ constrained ],
        text_big: "<b>De durée limitée, </b>l'exposition permet d'associer l'univers de la marque à des talents artistiques extérieurs pour un moment de partage culturel sur un mode proche du mécénat.",
        title_left: "Type de vente",
        text_left: "Bien choisir son espace de vente est important pour la suite de vos événements.",

        type: "Grand espace",

        title_right: "De l’espace pour votre clientèle, vos marchandises, l’article phare de votre marque.",
        text_right: "Selon ce que vous voulez présenter ou mettre en avant, la superficie reste un point important pour la recherche d’un emplacement.",
        item: [
            {
                title: "Durée</br> favorable :",
                value: "1 jour à 3 semaines"
            },
            {
                title: "Loyer</br> approximatif :",
                value: "25 à 200€ / jour"
            },
            {
                title: "Surface</br> commerciale :",
                value: "Prévoir un emplacement"
            }
        ]
    },
    {
        id: concept,
        class: [ text, cols-2 ],
        text_big: '<b>Ce qui est possible de faire avec ce </b>type de surface commerciale',
        text: 'A remplir'
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/formes/exposition-01.jpg"
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/formes/exposition-02.jpg"
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/formes/exposition-03.jpg"
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/formes/exposition-04.jpg"
    }
]
---
