---
title: "Les hôtels"
subtitle: ""
image_teaser: "/project/images/lieux/hotel.jpg"
image_background: "/project/images/lieux/hotel.jpg"
summary: "Contrairement aux bars et aux restaurants, une seule partie de la surfaces du lieu est exploitée et destinée à la vente du produit."
website: ""
year: 2017
slug: "hotel"
featured: true
tags: [ Événementialisé, Grande distribution, 1 jour à 3 semaines, Grand espace ]
filters: [ Lieu d'exploitation ]
paragraph: [
    {
        id: client,
        class: [ constrained ],
        text_big: "<b>Plus feutrés, </b>ces emplacements permettent la mise en avant de produits plus typés luxe.",
        title_left: "Type de vente",
        text_left: "Bien choisir son espace de vente est important pour la suite de vos événements.",

        type: "Espace événementiel",

        title_right: "De l’espace pour votre clientèle, vos marchandises, l’article phare de votre marque.",
        text_right: "Selon ce que vous voulez présenter ou mettre en avant, la superficie reste un point important pour la recherche d’un emplacement.",
        item: [
            {
                title: "Durée</br> favorable :",
                value: "1 jour à 3 semaines"
            },
            {
                title: "Loyer</br> approximatif :",
                value: "20 à 100€ / jour"
            },
            {
                title: "Surface</br> commerciale :",
                value: "1 à 50 m2"
            }
        ]
    },
    {
        id: concept,
        class: [ text, cols-2 ],
        text_big: "<b>Une totale créativité avec ce </b>type de d'implantation",
        text: 'A remplir'
    }
]
---
