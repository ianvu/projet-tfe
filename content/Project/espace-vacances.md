---
title: "Les espaces vacances"
subtitle: ""
image_teaser: "/project/images/lieux/plage.jpg"
image_background: "/project/images/lieux/plage.jpg"
summary: ""
website: ""
year: 2017
slug: "espace-vacances"
featured: true
tags: [ Événementialisé, Grande distribution, Indépendant, 1 jour à 3 semaines, Grand espace ]
filters: [ Lieu d'exploitation ]
paragraph: [
    {
        id: client,
        class: [ constrained ],
        text_big: "<b>A remplir",
        text_left: "Bien choisir son espace de vente est important pour la suite de vos événement.",

        type: "Espace événementiel",

        title_right: "De l’espace pour votre clientèle, vos marchandises, l’article phare de votre marque.",
        text_right: "Selon ce que vous voulez présenter ou mettre en avant, la supérficie compte et reste un point important à la recherche d’un emplacement.",
        item: [
            {
                title: "Durée</br> favorable :",
                value: "1 jour à 3 semaines"
            },
            {
                title: "Loyer</br> approximatif :",
                value: "Selon l’espace et l’environnement"
            },
            {
                title: "Surface</br> commerciale :",
                value: "Prévoir un emplacement"
            }
        ]
    },
    {
        id: concept,
        class: [ text, cols-2 ],
        text_big: "<b>Une totale créativité avec ce </b>type de d'implantation",
        text: 'A remplir'
    }
]
---
