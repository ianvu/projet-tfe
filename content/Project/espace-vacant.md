---
title: "Les espaces vacants"
subtitle: ""
image_teaser: "/project/images/lieux/espace-vacant.jpg"
image_background: "/project/images/lieux/espace-vacant.jpg"
summary: "Anciens ateliers, hangars désaffectés, boutiques vides, les zones urbaines fourmillent de lieux laissés vacants qui sont autant d'emplacements recherchés par les marques pour créer un pop-up store original."
website: ""
year: 2017
slug: "espace-vacant"
featured: true
tags: [ Événementialisé, Grande distribution, 1 mois à 3 mois, Destockage ]
filters: [ Lieu d'exploitation ]
paragraph: [
    {
        id: client,
        class: [ constrained ],
        text_big: "<b>Anciens ateliers, hangars désaffectés, boutiques vides, </b>les zones urbaines fourmillent de lieux laissés vacants qui sont autant d'emplacements recherchés par les marques pour créer un pop-up store original.",
        text_left: "Bien choisir son espace de vente est important pour la suite de vos événements.",

        type: "Espace commercial",

        title_right: "De l’espace pour votre clientèle, vos marchandises, l’article phare de votre marque.",
        text_right: "Selon ce que vous voulez présenter ou mettre en avant, la superficie reste un point important pour la recherche d’un emplacement.",
        item: [
            {
                title: "Durée</br> favorable :",
                value: "1 mois à 3 mois"
            },
            {
                title: "Loyer</br> approximatif :",
                value: "500 à 1500€ / mois"
            },
            {
                title: "Surface</br> commerciale :",
                value: "100 à 2000 m2"
            }
        ]
    },
    {
        id: concept,
        class: [ text, cols-2 ],
        text_big: "<b>Une totale créativité avec ce </b>type de d'implantation",
        text: 'A remplir'
    }
]
---
