---
title: "Les trucks"
subtitle: ""
image_teaser: "/project/images/formes/truck.jpg"
image_background: "/project/images/formes/truck.jpg"
summary: "Mobile par définition, le truck ressemble beaucoup dans l'idée aux étals ambulants des marchés."
website: ""
year: 2017
slug: "trucks"
featured: true
tags: [ Accessibilisé, Petit indépendant, Grande distribution, 1 jour à 3 semaines, Grand espace ]
filters: [ Structure commerciale ]
paragraph: [
    {
        id: client,
        class: [ constrained ],
        text_big: "<b>Mobile par définition,</b> le truck ressemble beaucoup dans l'idée aux étals ambulants des marchés.",
        title_left: "Type de vente",
        text_left: "Bien choisir son espace de vente est important pour la suite de vos événements.",

        type: "Grand Espace",

        title_right: "De l’espace pour votre clientèle, vos marchandises, l’article phare de votre marque.",
        text_right: "Selon ce que vous souhaitez présenter ou mettre en avant, la superficie est un point important pour la recherche d’un emplacement.",
        item: [
            {
                title: "Durée</br> favorable :",
                value: "1 jour à 3 semaines"
            },
            {
                title: "Loyer</br> approximatif :",
                value: "50 - 100€ / jour"
            },
            {
                title: "Surface</br> commerciale :",
                value: "Prévoir un emplacement"
            }
        ]
    },
    {
        id: concept,
        class: [ text, cols-2 ],
        text_big: '<b>Ce qui est possible de faire avec ce </b>type de surface commerciale',
        text: 'Parmi ses atouts, le truck est mobile et permet du coup de programmer des tournées bien orchestrées soutenues par des campagnes de communication passant notamment par les réseaux sociaux.'
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/formes/truck-01.jpg"
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/formes/truck-02.jpg"
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/formes/truck-03.jpg"
    },
    {
        class: [ constrained, image ],
        image_url: "/project/images/formes/truck-04.jpg"
    }
]
---
