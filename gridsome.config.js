// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config
const autoprefixer = require("autoprefixer")({
  grid: true,
});
const postcssPlugins = [autoprefixer];
// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: "Scheme Corner",
  siteUrl: "http://ianvu.com/projets/tfe",
  siteDescription: "",
  titleTemplate: "%s • Scheme Corner",
  pathPrefix: "/projets/tfe/",
  css: {
    loaderOptions: {
      postcss: {
        plugins: postcssPlugins,
      },
    },
  },
  transformers: {
    remark: {},
  },
  plugins: [
    {
      use: "@gridsome/source-filesystem",
      options: {
        path: "content/project/**/*.md",
        typeName: "Project",
      },
    },
    {
      use: "@gridsome/source-filesystem",
      options: {
        path: "content/client/**/*.md",
        typeName: "Client",
      },
    },
  ],
  templates: {
    Project: [
      {
        path: "/pop-up/:slug",
        component: "./src/templates/Project.vue",
      },
    ],
  },
};
