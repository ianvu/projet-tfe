# Static files

Add static files here. Files in this directory will be copied directly to `dist` folder during build. For example, /static/robots.txt will be located at https://yoursite.com/robots.txt.

This file should be deleted.

# Layout page project /.md

---

title: "ProjectName"
subtitle: "SubtitleProject"
image_teaser: ""
image_background: ""
summary: "Description Structure"
website: "WebsiteOfProject"
year: 2020
slug: layout
featured: false
tags: [ Accessibilité, Événementialisé, Petit indépendant, Grande distribution, 1 jour à 3 semaines, 1 mois à 3 mois, 3 mois à 6 mois, 6 mois à 1 an, Centre commercial, Local en ville, Grand espace, Destockage, ]
paragraph: [
{
id: client,
class: [ constrained ],
text_big: "<b>Client. Randomised words which don’t look.</b> If passage words you are going to use a passage words which don’t look of you need to be sure. When an unknown printer took a galley.",
item: [
{
title: "Durée favorable",
value: "1 jour à 3 semaines"
},
{
title: "Loyer approximatif",
value: "100 - 500€ / jours"
},
{
title: "Surface commerciale",
value: "50 à 100 m2"
}
]
},
{
class: [ constrained, image ],
image_url: ""
},
{
id: concept,
class: [ text, cols-2 ],
text_big: '<b>Digital world.</b> Randomised words.',
text: 'Randomised words which don’t look. If passage words you are going to use a passage words which don’t look of you need to be sure. When an unknown printer took a galley.'
},
{
class: [ full, text, full--with-text ],
text_big: "Concept. Randomised words which don’t look. If passage words you are going to use a passage words which don’t look of you need to be sure. When an unknown printer took a galley. Amet suscipit frig lacus. Pellentesque suscipit ante at ullamcorper pulvinar neque porttitor.",
text: "Randomised words which don’t look. If passage words you are going to use a passage words which don’t look of you need to be sure. When an unknown printer took a galley.",
image_url: ""
},
{
class: [ constrained, image ],
image_url: ""
},
{
id: services,
class: [ text, stats, cols-2 ],
text_big: "<b>Services.</b> Amet suscipit frig lacus pulvinar.",
text: "Amet suscipit frig lacus. Pellentesque suscipit ante at ullamcorper pulvinar neque porttitor.",
item: [
{
title: "JS - 6 year experience",
value: "40"
},
{
title: "HTML 5 - 7 year experience",
value: "60"
},
{
title: "Web design - 8 year experience",
value: "80"
}
]
},
{
class: [ constrained, image ],
image_url: ""
},
{
class: [ full ],
image_url: ""
},
{
id: video,
class: [ text, cols-2 ],
text_big: "<b>Video. Like.</b> Randomised words.",
text: "Randomised words which don’t look. If passage words you are going to use a passage words which don’t look of you need to be sure. When an unknown printer took a galley."
},
{
class: [ video ],
video_id: 43241044
},
{
id: result,
class: [ text, cols-2 ],
text_big: "<b>Result.</b> Amet suscipit frig lacus pulvinar.",
text: "Amet suscipit frig lacus pulvinar.",
item: [
{
title: "Art director",
value: "Digistrict"
},
{
title: "Developer",
value: "Victor Shibut"
},
{
title: "Designer",
value: "Maya Delia"
}
]
}
]

---
