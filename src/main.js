// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api
import "isomorphic-fetch";

import DefaultLayout from "~/layouts/Default.vue";
import VueFuse from "vue-fuse";
import VueScrollactive from "vue-scrollactive";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";

import {
  faFacebookF,
  // faInstagram,
  // faLinkedinIn,
  faTwitter,
  // faYoutube
} from "@fortawesome/fontawesome-free-brands";

import {
  faSearch,
  faLongArrowAltLeft,
  faArrowCircleRight,
  faAngleRight,
  faAngleDown,
  faArrowDown,
  faPaperPlane,
  faPhoneAlt,
  faChevronCircleLeft,
  faChevronCircleRight,
  faFileDownload,
  faExternalLinkAlt,
  faLongArrowAltRight,
  faArrowLeft,
  faArrowRight,
} from "@fortawesome/free-solid-svg-icons";

library.add(
  faFacebookF,
  faTwitter,

  faSearch,
  faLongArrowAltLeft,
  faArrowCircleRight,
  faAngleRight,
  faAngleDown,
  faArrowDown,
  faPaperPlane,
  faPhoneAlt,
  faChevronCircleLeft,
  faChevronCircleRight,
  faFileDownload,
  faExternalLinkAlt,
  faLongArrowAltRight,
  faArrowLeft,
  faArrowRight
);

export default function(Vue, { head }) {
  // Set default layout as a global component
  Vue.component("Layout", DefaultLayout);
  Vue.component("font-awesome-icon", FontAwesomeIcon);

  Vue.use(VueFuse);
  Vue.use(VueScrollactive);

  head.htmlAttrs = { lang: "fr" };
}
